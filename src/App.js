import React, { Component } from 'react'
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom'

import KanbanBoardContainer from './Kanban/KanbanBoardContainer'
import EditCard from './Kanban/EditCard'

import createHistory from 'history/createBrowserHistory'
const history = createHistory()

export default class App extends Component {
    render() {
        return (
            <Router history={history}>
                <div>
                    <Route path="/" component={KanbanBoardContainer} />
                    <Route path="/edit/:card_id" component={EditCard} />
                </div>
            </Router>
        )
    }
}
