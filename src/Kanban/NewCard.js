import React, { Component } from 'react'
import PropTypes from 'prop-types'
import CardForm from './CardForm'

export default class NewCard extends Component {
    componentWillMount() {
        this.setState({
            id: Date.now(),
            title: '',
            description: '',
            status: 'todo',
            color: '#c9c9c9',
            tasks: []
        })
    }

    handleChange(field, value) {
        this.setState({[field]: value})
    }

    handleSubmit(e) {
        e.preventDefault()
        this.props.cardCallbacks.addCard(this.state)
        this.setState({
            id: Date.now(),
            title: '',
            description: '',
            status: 'todo',
            color: '#c9c9c9',
            tasks: []
        })
    }

    render() {
        const modal = {
            id: 'new_card',
            title: 'Add new card'
        }
        return (
            <CardForm draftCard={this.state}
                buttonLabel="Create card"
                modal={modal}
                handleChange={this.handleChange.bind(this)}
                handleSubmit={this.handleSubmit.bind(this)}
            />
        )
    }
}

NewCard.propTypes = {
    cardCallbacks: PropTypes.object
}
