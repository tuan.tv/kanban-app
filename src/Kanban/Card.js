import React, { Component } from 'react'
import PropTypes from 'prop-types'
import marked from 'marked'
import { DragSource, DropTarget } from 'react-dnd'
import { Link } from 'react-router-dom'
// import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import CheckList from './CheckList'
import constants from '../constants'

let titlePropType = (props, propName, componentName) => {
    if(props[propName]) {
        let value = props[propName]
        if(typeof value !== 'string' || value.length > 10) {
            return new Error(
                `${propName} in ${componentName} is longer than 80 characters`
            )
        }
    }
}

let cardDragSpec = {
    beginDrag(props) {
        return {
            id: props.id,
            status: props.status
        }
    },
    endDrag(props) {
        props.cardCallbacks.persistCardDrag(props.id, props.status)
    }
}

let cardDropSpec = {
    hover(props, monitor) {
        const draggedId = monitor.getItem().id
        props.cardCallbacks.updatePosition(draggedId, props.id)
    }
}

let collectDrag = (connect, monitor) => {
    return {
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging()
    }
}

let collectDrop = (connect, monitor) => {
    return {
        connectDropTarget: connect.dropTarget()
    }
}

class Card extends Component {
    constructor() {
        super()
        this.state = {
            showDetails: false
        }
        this.toggleDetails = this.toggleDetails.bind(this)
    }

    toggleDetails() {
        this.setState({showDetails: !this.state.showDetails})
    }

    render() {
        const { connectDragSource, connectDropTarget, isDragging } = this.props

        let cardDetails = ''
        let classTitle = 'card__title'
        if(this.state.showDetails) {
            classTitle = 'card__title card__title--is-open'
            cardDetails = <div className="card__details">
                            <div dangerouslySetInnerHTML={{__html:marked(this.props.description)}} />
                            <CheckList 
                                cardId={this.props.id} 
                                tasks={this.props.tasks}
                                taskCallbacks={this.props.taskCallbacks}
                                cardCallbacks={this.props.cardCallbacks}
                            />
                        </div>
        }

        let sideColor = {
            position: 'absolute',
            zIndex: -1,
            top: 0,
            bottom: 0,
            left: 0,
            width: 5,
            backgroundColor: this.props.color
        }

        const cardClass = isDragging ? 'card card--is-dragging' : 'card'

        return connectDropTarget(connectDragSource(
            <div className={cardClass}>
                <div className="card__border-left" style={sideColor}/>
                <div className="card__edit"><Link to={'/edit/'+this.props.id}>&#9998;</Link></div>
                <div className={classTitle}
                    onClick={this.toggleDetails}>
                    {this.props.title}
                </div>
                {cardDetails}
            </div>
        ))
    }
}

Card.PropTypes = {
    id: PropTypes.number,
    title: titlePropType,
    description: PropTypes.string,
    color: PropTypes.string,
    tasks: PropTypes.arrayOf(PropTypes.object),
    taskCallbacks: PropTypes.object,
    cardCallbacks: PropTypes.object,
    connectDragSource: PropTypes.func.isRequired,
    connectDropTarget: PropTypes.func.isRequired,
    isDragging: PropTypes.bool.isRequired
}

const dragHighOrderCard = DragSource(constants.CARD, cardDragSpec, collectDrag)(Card)
const dragDropHighOrderCard = DropTarget(constants.CARD, cardDropSpec, collectDrop)(dragHighOrderCard)

export default dragDropHighOrderCard
