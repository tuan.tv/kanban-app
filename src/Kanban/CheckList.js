import React, { Component } from 'react'
import PropTypes from 'prop-types'

class CheckList extends Component {
    handleClick() {
        this.refs.inputSearch.focus()
    }

    checkInputKeyPress(event) {
        if(event.key === 'Enter') {
            this.props.taskCallbacks.add(this.props.cardId, event.target.value)
            event.target.value = ''
        }
    }

    render() {
        let tasks = this.props.tasks.map((task, index) => (
            <li key={index} className="checklist__task">
                <input type="checkbox" className="checklist__task-input" defaultChecked={task.done}
                    onChange={
                        this.props.taskCallbacks.toggle.bind(null, this.props.cardId, task.id, index)
                    }
                />
                <span className={task.done ? 'checklist__task--done' : ''}>{task.name} {' '}</span>
                <button className="checklist__task--remove" onClick={
                    this.props.taskCallbacks.delete.bind(null, this.props.cardId, task.id, index)
                }> </button>
            </li>
        ))

        return (
            <div className="checklist">
                <ul>{tasks}</ul>
                <input type="text"
                    ref="inputSearch"
                    className="checklist--add-task"
                    placeholder="Type then hit enter to add a new task"
                    onKeyPress={this.checkInputKeyPress.bind(this)}
                />
                <button className="btn btn-default" onClick={this.handleClick.bind(this)}>Focus the text input</button>
            </div>
        )
    }
}

CheckList.PropTypes = {
    cardId: PropTypes.number,
    tasks: PropTypes.arrayOf(PropTypes.object)
}

export default CheckList
