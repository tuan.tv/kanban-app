import React, { Component } from 'react'
import PropTypes from 'prop-types'
import CardForm from './CardForm'

export default class EditCard extends Component {
    componentWillMount() {
        let card = this.props.cards.find((card) => card.id === this.props.match.params.id)
        this.setState({...card})
    }

    handleChange(field, value) {
        this.setState({[field]: value})
    }

    handleSubmit(e) {
        e.preventDefault()
        this.props.cardCallbacks.updateCard(this.state)
    }

    render() {
        return (
            <CardForm draftCard={this.state}
                buttonLabel="Edit card"
                modal="edit_card"
                handleChange={this.handleChange.bind(this)}
                handleSubmit={this.handleSubmit.bind(this)}
                handleClose={this.handleClose.bind(this)}
            />
        )
    }
}

EditCard.propTypes = {
    cardCallbacks: PropTypes.object
}
