import React, { Component } from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'
import Card from './Card'
import { DropTarget } from 'react-dnd'
import constants from '../constants'

let listTargetSpec = {
    hover(props, monitor) {
        const draggedId = monitor.getItem().id
        props.cardCallbacks.updateStatus(draggedId, props.id)
    }
}

let collect = (connect, monitor) => {
    return {
        connectDropTarget: connect.dropTarget()
    }
}

class List extends Component {
    constructor() {
        super()
        this.renderCards = this.renderCards.bind(this)
    }

    renderCards(cards, spinner) {
        if(_.isEmpty(cards))
            return <div dangerouslySetInnerHTML={{__html: spinner}}></div>
        return cards
    }

    render() {
        const { connectDropTarget } = this.props

        var cards = this.props.cards.map((card, index) => {
            return <Card key={index}
                id={card.id}
                title={card.title}
                description={card.description}
                tasks={card.tasks}
                taskCallbacks={this.props.taskCallbacks}
                cardCallbacks={this.props.cardCallbacks}
            />
        })

        const spinner = '<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i> <span class="sr-only">Loading...</span>'

        return connectDropTarget(
            <div className={`list ${this.props.className}`}>
                <h1 className="list__title">{this.props.title}</h1>
                {this.renderCards(cards, spinner)}
            </div>
        )
    }
}

List.PropTypes = {
    title: PropTypes.string.isRequired,
    cards: PropTypes.arrayOf(PropTypes.object),
    taskCallbacks: PropTypes.object,
    cardCallbacks: PropTypes.object,
    connconnectDropTarget: PropTypes.func.isRequiredect
}

export default DropTarget(constants.CARD, listTargetSpec, collect)(List)
