import React, { Component } from 'react'
import update from 'react-addons-update'
import KanbanBoard from './KanbanBoard'
import { API_URL, API_HEADERS } from '../api.config'
import { throttle } from '../untils'
import 'babel-polyfill'

class KanbanBoardContainer extends Component {
    constructor() {
        super()
        this.state = {
            cards: []
        }
        this.addTask = this.addTask.bind(this)
        this.deleteTask = this.deleteTask.bind(this)
        this.toggleTask = this.toggleTask.bind(this)
        this.updateCardStatus = throttle(this.updateCardStatus.bind(this))
        this.updateCardPosition = throttle(this.updateCardPosition.bind(this), 500)
        this.persistCardDrag = this.persistCardDrag.bind(this)
        this.addCard = this.addCard.bind(this)
        this.updateCard = this.updateCard.bind(this)
    }

    componentDidMount() {
        fetch(`${API_URL}/cards`, {headers: API_HEADERS})
        .then((res) => res.json())
        .then((data) => {
            console.log(data)
            this.setState({cards: data})
        })
        .catch((error) => console.log(error))
    }

    addTask(cardId, taskName) {
        let cardIndex = this.state.cards.findIndex((card) => card.id === cardId)
        let newTask = {
            id: Date.now(),
            name: taskName,
            done: false
        }
        let nextState = update(this.state.cards, {
            [cardIndex]: {
                tasks: { $push: [newTask] }
            }
        })
        this.setState({cards: nextState})

        // Call the API to add the task on the server
        fetch(`${API_URL}/cards/${cardId}/tasks`, {
            method: 'post',
            headers: API_HEADERS,
            body: JSON.stringify(newTask)
        })
        .then((response) => response.json())
        .then((responseData) => {
            newTask.id = responseData.id
            this.setState({cards: nextState})
        })
    }

    deleteTask(cardId, taskId, taskIndex) {
        let cardIndex = this.state.cards.findIndex((card) => card.id === cardId)
        let nextState = update(this.state.cards, {
            [cardIndex]: {
                tasks: {$splice: [[taskIndex, 1]]}
            }
        })
        this.setState({cards: nextState})

        // Call the API to remove the task on the server
        fetch(`${API_URL}/cards/${cardId}/tasks/${taskId}`, {
            method: 'delete',
            headers: API_HEADERS
        })
    }

    toggleTask(cardId, taskId, taskIndex) {
        let cardIndex = this.state.cards.findIndex((card) => card.id === cardId)
        let newDoneValue
        let nextState = update(this.state.cards, {
            [cardIndex]: {
                tasks: {
                    [taskIndex]: {
                        done: { $apply: (done) => {
                            newDoneValue = !done
                            return newDoneValue
                        } }
                    }
                }
            }
        })
        this.setState({cards: nextState})

        // Call the API to toggle the task on the server
        fetch(`${API_URL}/cards/${cardId}/tasks/${taskId}`, {
            method: 'put',
            headers: API_HEADERS,
            body: JSON.stringify({done:newDoneValue})
        })
    }

    updateCardStatus(cardId, listId) {
        let cardIndex = this.state.cards.findIndex((card) => card.id === cardId)
        let card = this.state.cards[cardIndex]

        if(card.status !== listId) {
            this.setState(update(this.state, {
                cards: {
                    [cardIndex]: {
                        status: { $set: listId }
                    }
                }
            }))
        }
    }

    updateCardPosition(cardId, afterId) {
        if(cardId !== afterId) {
            let cardIndex = this.state.cards.findIndex((card) => card.id === cardId)
            let card = this.state.cards[cardIndex]
            let afterIndex = this.state.cards.findIndex((card) => card.id === afterId)

            this.setState(update(this.state, {
                cards: {
                    $splice: [
                        [cardIndex, 1],
                        [afterIndex, 0, card]
                    ]
                }
            }))
        }
    }

    persistCardDrag(cardId, status) {
        let cardIndex = this.state.cards.findIndex((card) => card.id === cardId)
        let card = this.state.cards[cardIndex]

        fetch(`${API_URL}/cards/${cardId}`, {
            method: 'put',
            headers: API_HEADERS,
            body: JSON.stringify({status: card.status, row_order_position: cardIndex})
        })
        .then((response) => {
            if(!response.ok) {
                throw new Error('Server response wasn\'t OK')
            }
        })
        .catch((error) => {
            console.log(error)
            this.setState(update(this.state, {
                cards: {
                    [cardIndex]: {
                        status: { $set: status }
                    }
                }
            }))
        })
    }

    addCard(card) {
        let prevState = this.state
        
        if(card.id === null) {
            card = Object.assign({}, card, {id: Date.now()})
        }

        let nextState = update(this.state.cards, { $push: [card] })

        this.setState({cards: nextState})

        fetch(`${API_URL}/cards`, {
            method: 'POST',
            headers: API_HEADERS,
            body: JSON.stringify(card)
        })
        .then((response) => {
            if(response.ok) {
                response.json()
            } else {
                throw new Error('Server response wasn\'n Ok')
            }
        })
        .then((data) => {
            card.id = data.id
            this.setState({cards: nextState})
        })
        .catch((error) => {
            console.log(error)
            this.setState({prevState})
        })
    }

    updateCard(card) {
        let prevState = this.state

        let cardIndex = this.state.cards.findIndex((c) => c.id === card.id)

        let nextState = update(
            this.state.cards, {
                [cardIndex]: { $set: card }
            }
        )

        this.setState({cards: nextState})

        fetch(`${API_URL}/cards/${card.id}`, {
            method: 'PUT',
            headers: API_HEADERS,
            body: JSON.stringify(card)
        })
        .then((response) => {
            if(!response.ok) {
                throw new Error('Server response wasn\'t Ok')
            }
        })
        .catch((error) => {
            console.log(error)
            this.setState({cards: prevState})
        })
    }

    render() {
        return (
            <div>
                <KanbanBoard cards={this.state.cards}
                    taskCallbacks={{
                        add: this.addTask,
                        toggle: this.toggleTask,
                        delete: this.deleteTask
                    }}
                    cardCallbacks={{
                        addCard: this.addCard,
                        updateCard: this.updateCard,
                        updateStatus: this.updateCardStatus,
                        updatePosition: this.updateCardPosition,
                        persistCardDrag: this.persistCardDrag
                    }}
                />
            </div>
        )
    }
}

export default KanbanBoardContainer
