import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { DragDropContext } from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'
import List from './List'
import NewCard from './NewCard'

class KanbanBoard extends Component {
    constructor() {
        super()

        this.state = {
            open: false,
            bg: ''
        }

        this.handleChangeInput = this.handleChangeInput.bind(this)
    }

    handleOpen() {
        this.setState({open: !this.state.open})
    }

    handleChangeInput(e) {
        this.setState({bg: e.target.value})
        console.log(e.target.value)
    }

    render(){
        const bg = this.state.bg

        const appBackground = {
            backgroundImage: `url(${bg})`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center'
        }

        const appClass = this.state.open ? 'app_bg app_bg--open' : 'app_bg'

        return (
            <div className="app" style={appBackground}>
                <div className={appClass}>
                    <div className="app_config" onClick={this.handleOpen.bind(this)}>
                        <i className="fa fa-cog" aria-hidden="true"></i>
                    </div>
                    <label>
                        <h5>App background</h5>
                        <span>Paste url image to input</span>
                        <input onChange={this.handleChangeInput} type="text" className="form-control"/>
                    </label>
                </div>

                <button className="float-button" data-toggle="modal" data-target="#new_card"></button>
                <NewCard cardCallbacks={this.props.cardCallbacks} />

                <List id='todo' className="list--todo" title="To Do"
                    taskCallbacks={this.props.taskCallbacks}
                    cardCallbacks={this.props.cardCallbacks}
                    cards={
                        this.props.cards.filter((card) => card.status === "todo")
                } />

                <List id='in-progress' className="list--in-progress" title="In Progress" 
                    taskCallbacks={this.props.taskCallbacks}
                    cardCallbacks={this.props.cardCallbacks}
                    cards={
                        this.props.cards.filter((card) => card.status === "in-progress")
                } />

                <List id='done' className="list--done" title='Done' 
                    taskCallbacks={this.props.taskCallbacks}
                    cardCallbacks={this.props.cardCallbacks}
                    cards={
                        this.props.cards.filter((card) => card.status === "done")
                } />
            </div>
        )
    }
}

KanbanBoard.PropTypes = {
    cards: PropTypes.arrayOf(PropTypes.object),
    taskCallbacks: PropTypes.object,
    cardCallbacks: PropTypes.object
}

export default DragDropContext(HTML5Backend)(KanbanBoard)
