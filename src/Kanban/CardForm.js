import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class CardForm extends Component {
    handleChange(field, event) {
        this.props.handleChange(field, event.target.value)
    }

    render(){
        return (
            <div className="modal fade" id={this.props.modal.id} tabIndex="-1" role="dialog" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">{this.props.modal.title}</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <form action="POST" onSubmit={this.props.handleSubmit.bind(this)}>
                            <input className="form-control"
                                type='text'
                                value={this.props.draftCard.title}
                                onChange={this.handleChange.bind(this,'title')}
                                placeholder="Title"
                                required={true}
                                autoFocus={true} />

                            <textarea className="form-control"
                                value={this.props.draftCard.description}
                                onChange={this.handleChange.bind(this,'description')}
                                placeholder="Description"
                                required={true} />

                            <select id="status" className="custom-select"
                                value={this.props.draftCard.status}
                                onChange={this.handleChange.bind(this,'status')}>
                                <option value="todo">To Do</option>
                                <option value="in-progress">In Progress</option>
                                <option value="done">Done</option>
                            </select>
                            <br />

                            <input id="color"
                                value={this.props.draftCard.color}
                                onChange={this.handleChange.bind(this,'color')}
                                type="color" />

                            <div className="actions">
                                 <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" className="btn btn-success">{this.props.buttonLabel}</button>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        )
    }
}

CardForm.propTypes = {
    buttonLabel: PropTypes.string.isRequired,
    modal: PropTypes.object.isRequired,
    draftCard: PropTypes.shape({
        title: PropTypes.string,
        description: PropTypes.string,
        status: PropTypes.string,
        color: PropTypes.string
    }).isRequired,
    handleChange: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired
}
